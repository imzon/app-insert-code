function shareToInlineFacebook() {
  window.open("https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(window.location.href), "_blank", "height=480,width=640");
}

function shareToInlineTwitter() {
  window.open("https://twitter.com/share?url="+encodeURIComponent(window.location.href), "_blank", "height=480,width=640");
}

function shareToInlineReddit() {
  window.open("https://www.reddit.com/submit?url="+encodeURIComponent(window.location.href), "_blank", "height=480,width=640");
}

function shareToInlinePinterest() {
  window.open("http://pinterest.com/pin/create/link/?url="+encodeURIComponent(window.location.href), "_blank", "height=480,width=640");
}

function shareToInlineEmail() {
  window.open("mailto:?body="+encodeURIComponent(window.location.href));
}

function wezuShareTo(type) {
  switch (type) {
    case 'facebook':
      shareToInlineFacebook();
      break;
    case 'twitter':
      shareToInlineTwitter();
      break;
    case 'reddit':
      shareToInlineReddit();
      break;
    case 'pinterest':
      shareToInlineReddit();
      break;
    default:
      break;
  }
}