<?php

namespace App\Http\Controllers;

use App\Sevices\ThemeAsset;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function getSetting(Request $request)
    {
        $user = auth()->user();
        $setting = $user->setting;
        if (empty($setting)) {
            $setting = $user->setting()->create([
                'is_active' => false,
                'meta_key' => 'setting',
                'meta_value' => [
                    'position' => 'bottom-right',
                    'background' => '#FF9800',
                    'icon' => 1,
                    'icon_color' => '#333333',
                ]
            ]);
        }
        return response()->json($setting);
    }
    public function saveSetting(Request $request)
    {
        $user = auth()->user();
        $setting = $user->setting;
        if (empty($setting)) {
            $setting = $user->setting()->create($request->all());
        } else {
            $setting->fill($request->all());
            $setting->save();
        }
        $themeAsset = new ThemeAsset($user);
        if ($setting->is_active) {
            $themeAsset->installThemeSnippet('xyz-header', '</head>', $setting->meta_value['header']);
            $themeAsset->installThemeSnippet('xyz-footer', '</body>', $setting->meta_value['footer']);
        } else {
            $themeAsset->uninstallThemeSnippet('xyz-header');
            $themeAsset->uninstallThemeSnippet('xyz-footer');
        }

        return response()->json(['success' => true], 201);
    }
    public function testing()
    {
        $user = auth()->user();
        $themeAsset = new ThemeAsset($user);
//        $themeAsset->installThemeSnippet('xyz-header', '<script>alert("1")</script>');
        $themeAsset->uninstallThemeSnippet('xyz-header');
        //get current theme
//        $themeId = '128540737757';
//        //create snippet
//        $name = 'imz-header';
//        $this->removeSnippet($user, $themeId, $name);
//        $request = $user->api()->rest('PUT', '/admin/api/2021-10/themes/' . $themeId . '/assets.json', [
//            'asset' => [
//                'key' => 'snippets/xyz-header.liquid',
//                'value' => '<style>body{background: red;}.xyz{color: blue}</style>',
//            ]
//        ]);
//        dd($request['body']);
        //append to theme
//        $append = "{% include '${$name}' %}\n";
//        $this->appendTheme($user, $themeId, $append);
        return true;
    }

}
