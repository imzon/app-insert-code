<?php


namespace App\Sevices;


use App\Models\User;

class ThemeAsset
{
    /**
     * @var User $shop
     */
    public $shop;
    public $currentThemeId;

    public function __construct(User $shop)
    {
        $this->shop = $shop;
        $this->currentThemeId = $this->getCurrentTheme();
    }

    public function getCurrentTheme()
    {
        $themes = $this->shop->api()->rest('GET', '/admin/api/2021-10/themes.json')['body']['themes'];
        $currentTheme = null;
        foreach ($themes as $theme) {
            if ($theme->role == 'main') {
                $currentTheme = $theme;
                break;
            }
        }
        if (empty($currentTheme)) {
            throw new \LogicException('Invalid theme id');
        }
        return $currentTheme->id;
    }

    private function parseSnippetCode($snippetName)
    {
        return "{% include '$snippetName' %}\n";
    }

    public function createSnippet($snippetName, $content = null)
    {
        $path = 'snippets/' . $snippetName . '.liquid';
        $this->shop->api()->rest('PUT', '/admin/api/2021-10/themes/' . $this->currentThemeId . '/assets.json', [
            'asset' => [
                'key' => $path,
                'value' => $content,
            ],
        ]);

        return [
            'name' => $snippetName,
            'path' => $path,
            'code' => $this->parseSnippetCode($snippetName),
        ];
    }

    public function removeSnippet($name)
    {
        $path = 'snippets/' . $name . '.liquid';
        $this->shop->api()->rest('DELETE', '/admin/api/2021-10/themes/' . $this->currentThemeId . '/assets.json', [
            "asset" => [
                'key' => $path,
            ],
        ]);
        return true;
    }

    public function getThemeHTML($path = 'layout/theme.liquid')
    {
        return $this->shop->api()->rest('GET', '/admin/api/2021-10/themes/' . $this->currentThemeId . '/assets.json', [
            "asset" => [
                'key' => $path,
            ],
        ])['body']['asset']['value'];
    }

    public function appendThemeSnippet($snippetName, $htmlTag = '</head>')
    {
        $html = $this->getThemeHTML();
        $appendSnippet = $this->parseSnippetCode($snippetName);
        if (strpos($html, $appendSnippet) === false) {
            $pos = strpos($html, $htmlTag);
            $newHtml = substr($html, 0, $pos) . $appendSnippet . substr($html, $pos);
            $this->shop->api()->rest('PUT', '/admin/api/2021-10/themes/' . $this->currentThemeId . '/assets.json', [
                'asset' => [
                    'key' => 'layout/theme.liquid',
                    'value' => $newHtml,
                ],
            ])['body'];
            return true;
        }

        return true;
    }

    public function removeThemeSnippet($snippetName)
    {
        $html = $this->getThemeHTML();
        $appendedSnippet = $this->parseSnippetCode($snippetName);
        if (strpos($html, $appendedSnippet) !== false) {
            $pos = strpos($html, $appendedSnippet);
            $newHtml = substr($html, 0, $pos) . substr($html, $pos + strlen($appendedSnippet));
            $this->shop->api()->rest('PUT', '/admin/api/2021-10/themes/' . $this->currentThemeId . '/assets.json', [
                'asset' => [
                    'key' => 'layout/theme.liquid',
                    'value' => $newHtml,
                ],
            ]);
            return true;
        }
        return true;
    }

    public function uninstallThemeSnippet($snippetName)
    {
        $this->removeThemeSnippet($snippetName);
        $this->removeSnippet($snippetName);
        return true;
    }

    public function installThemeSnippet($snippetName, $htmlTag = '</head>', $content = null)
    {
        $this->createSnippet($snippetName, $content);
        $this->appendThemeSnippet($snippetName, $htmlTag);
        return true;
    }
}
