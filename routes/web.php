<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['verify.shopify', 'billable']], function () {
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('settings', [\App\Http\Controllers\SettingController::class, 'getSetting']);
    Route::post('settings', [\App\Http\Controllers\SettingController::class, 'saveSetting']);
    Route::post('testing', [\App\Http\Controllers\SettingController::class, 'testing']);
});

Route::get('/user', function () {
    $user = auth()->user();
//    $request = $user->api()->rest('POST', '/admin/api/2021-10/script_tags.json', [
//        "script_tag" => [
//            "event" => "onload",
//            "src" => secure_asset('/js/kim.js')
//        ]
//    ]);
//    $request = $user->api()->rest('GET', '/admin/api/2021-10/script_tags/198162612453.json');
    $request = $user->api()->rest('PUT', '/admin/api/2021-10/script_tags/198162612453.json', [
        "script_tag" => [
            "event" => "onload",
            "src" => secure_asset('/js/kim.js'),
        ],
    ]);
    \Illuminate\Support\Facades\Log::info($request);
    return $user;
//    $user->tokens()->delete();
//    $accessToken = $user->createToken('api')->plainTextToken;
//    return view('welcome', compact('accessToken'));
})->middleware(['verify.shopify', 'billable']);

Route::get('/privacy-policy', function () {
    return view('privacy');
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

Route::get('scripts/{id}/{version}.js', function ($id) {
    $setting = \App\Models\Setting::find($id);
    return response()->view('scripts', compact('setting'))->header('Content-Type', 'application/javascript');
})->name('script_tag');
