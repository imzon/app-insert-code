@extends('shopify-app::layouts.default')

@section('styles')
    @parent
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .form-zero {
            display: none;
        }
        .form-zero.loaded {
            display: block;
        }
    </style>
@endsection

@section('content')
    <div
        x-data="contactForm()"
        style="background-color: rgb(246, 246, 247); color: rgb(32, 34, 35); min-height: 100vh; padding-top: 25px;"
    >
        <div class="container">
            <h3>Insert Code</h3>
            <div class="card">
                <div class="card-body">
                    <p x-text="message"></p>
                    <form @submit.prevent="submitData" class="form-zero" :class="hasInit && 'loaded'">
                        <div class="mb-3 form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" x-model="is_active">
                            <label class="form-check-label" for="flexSwitchCheckChecked">Disable/Enable</label>
                        </div>
                        <div class="mb-3">
                            <label for="position" class="form-label">Header</label>
                            <textarea id="code-header" class="form-control" type="color" x-model="meta_value.header" rows="5"></textarea>
                            <p>The code will be printed in the <code>{{ '</head>' }}</code> section.</p>
                        </div>
                        <div class="mb-3">
                            <label for="position" class="form-label">Footer</label>
                            <textarea id="code-footer" class="form-control" type="color" x-model="meta_value.footer" rows="5"></textarea>
                            <p>The code will be printed above the <code>{{ '</body>' }}</code> tag.</p>
                        </div>
                        <button type="submit" class="btn btn-success" x-text="buttonLabel" :disabled="loading">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        actions.TitleBar.create(app, {title: 'Dashboard'});
    </script>
    <script>
        function contactForm() {
            return {
                hasInit: false,
                loading: true,
                buttonLabel: 'Save',
                is_active: false,
                meta_value: {
                    header: '',
                    footer: '',
                },
                message: '',
                getInit(loop = 1) {
                    if (loop >= 3) {
                        this.loading = false;
                        return false;
                    }
                    this.loading = true;
                    this.buttonLabel = 'Loading'
                    window.axios.get('/settings').then((res) => {
                        const data = res.data;
                        this.is_active = data.is_active;
                        this.meta_value = data.meta_value;
                        this.message = '';
                        this.hasInit = true;
                        this.loading = false;
                        this.buttonLabel = 'Save'
                    }).catch(() => {
                        setInterval(() => {
                            loop += 1;
                            this.getInit(loop)
                        }, 5000)
                    })
                },
                init() {
                    window.onload = () => {
                        this.getInit(1)
                    }
                },
                submitData() {
                    this.buttonLabel = 'Saving...'
                    this.loading = true;
                    window.axios.post('/settings', {
                        is_active: this.is_active,
                        meta_value: this.meta_value,
                    }).then(() => {
                        this.message = ''
                        let Toast = actions.Toast;
                        const toastNotice = Toast.create(app, {
                            message: 'Setting Saved',
                            duration: 5000,
                        });
                        toastNotice.dispatch(Toast.Action.SHOW);
                    }).catch(() => {
                        this.message = ''
                        let Toast = actions.Toast;
                        const toastNotice = Toast.create(app, {
                            message: 'Ooops! Something went wrong!',
                            duration: 5000,
                            isError: true,
                        });
                        toastNotice.dispatch(Toast.Action.SHOW);
                    }).finally(() => {
                        this.loading = false;
                        this.buttonLabel = 'Save'
                    })
                }
            }
        }
    </script>
@endsection
